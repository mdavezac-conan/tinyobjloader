cmake_minimum_required(VERSION 3.7)

project(tinyobjloader_test CXX)


set(CMAKE_C_STANDARD 99)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup(TARGETS)

add_executable(testme test.cpp)
target_link_libraries(testme CONAN_PKG::tinyobjloader)
