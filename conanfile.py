from conans import ConanFile, tools


class TinyobjloaderConan(ConanFile):
    name = "tinyobjloader"
    version = "1.0.60"
    license = "MIT"
    url = "https://gitlab.com/mdavezac-conan/tinyobjloader"
    homepage = "https://github.com/syoyo/tinyobjloader"
    description = "Tiny but powerful single file wavefront obj loader"
    topics = ("Computer Graphics", "Mesh", "Viewer")
    no_copy_source = True

    def source(self):
        git = tools.Git(folder="tinyobjloader")
        git.clone(self.homepage, branch="v1.0.6")

    def package(self):
        self.copy("tinyobjloader/tiny_obj_loader.h", dst="include")
        
    def package_id(self):
        self.info.header_only()
